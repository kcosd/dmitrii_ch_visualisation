import matplotlib.pyplot as plt

x_values = list(range(0, 6))
y_values = [x ** 3 for x in x_values]

plt.style.use('seaborn')  # STYLE
fig, ax = plt.subplots()
ax.plot(y_values, linewidth=3, c=(0, 0.8, 0))  # толщина линии

# Назначение заголовка диаграммы и меток осей.
ax.set_title("Cube Numbers", fontsize=24)
ax.set_xlabel("Value", fontsize=14)
ax .set_ylabel("Cube of Value", fontsize=14)


# Назначение размера шрифта делений на осях.
ax.tick_params(axis='both', labelsize=14)  # оформление делений на осях

# Назначение диапазона для каждой оси.
ax.axis([0, 5, 0, 160])

plt.show()
